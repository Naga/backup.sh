#!/bin/bash

############################# CONFIG START ####################################
DATE_STR=`date +%d-%m-%Y_%H-%M`
HOSTNAME=`hostname`
BACKUP_FILE_NAME=".backup_files" #list of files to pack in the backup, has to be in $USER's home
BACKUP_TXT="[$(tput setaf 6)Backup$(tput sgr0)] "



ENCRYPT_BACKUP="yes" #yes / no
CRYPT_KEY_FILE="/root/.ssh/backup_key.pub"

BACKUP_TYPE="folder" #types: ftp, folder (default: folder)

#folder:
BACKUP_FOLDER="/mnt/backups"

#ftp:
FTP_HOST="127.0.0.1"
FTP_USER="user"
FTP_PASS="pass"
FTP_FOLDER="backups"

#backup_rotation
ROTATION_ENABLED="yes" #yes / no
ROTATION_TYPE="combined" #valid: filecount (for maximum number of backups), size (for having a maximum size of all backup files), combined (maximum number of files but less files if over size maximum)
MAX_SIZE="1Gb"
MAX_FILES=20


################################ CONFIG END ###################################

# am i root?
 
if [ "$UID" != "0" ]
then
	echo "This script must be run as root!"
	exit 1
fi

# calculate size in bytes, strip types(mb/b/kb/etc)
if [ "${ROTATION_TYPE}" != "filecount" ]; then
	SIZE_TYPE=`echo ${MAX_SIZE: -2} | tr '[:upper:]' [:lower:]`
	case ${SIZE_TYPE} in
		"kb")
			CONV=1024
			;;
		"mb")
			CONV=1048576
			;;
		"gb")
			CONV=1073741824
			;;
		"tb")
			CONV=1099511627776
			;;
		*)
			CONV=0
	esac
	SIZE_NUM=`echo $MAX_SIZE | tr "[:upper:]" "[:lower:]" | sed "s/${SIZE_TYPE}//g"`
	BACKUP_MAX_SIZE=$(($SIZE_NUM*$CONV))
fi


#again todo: better way to find users
USERS="`cat /etc/passwd | awk -F : '{if ($7 != "/bin/false" && $7 != "/usr/sbin/nologin" && !system("test -d "$6))  print $1" "$6}'`"
echo "$USERS" | while IFS=' ' read -ra NEXT_USER
do
	USER=${NEXT_USER[0]}
	USER_HOME=${NEXT_USER[1]}

	
	if [ ${ENCRYPT_BACKUP} == "yes" ]; then
		TAR_BACKUP_FILENAME="backup_${HOSTNAME}-${USER}_${DATE_STR}.enc"
		ENC_TEXT="and encrypting "
	else
		TAR_BACKUP_FILENAME="backup_${HOSTNAME}-${USER}_${DATE_STR}.tar.xz"
		ENC_TEXT=""
	fi
	
	
	if [ ! -e ${USER_HOME}/${BACKUP_FILE_NAME} ]; then
		echo ${BACKUP_TXT}"User ${USER} hasn't defined any files for backup, skipping."
		continue
	fi

	echo "${BACKUP_TXT}Starting backup for ${USER}"	

	rm -rf ${USER_HOME}/_backup 2> /dev/null
	
#run part of script as ${USER}
su -m ${USER} <<EOF
	mkdir ${USER_HOME}/_backup > /dev/null 2>&1
	cd ${USER_HOME}/_backup

	if [[ ( -f "${USER_HOME}/pre_backup.sh" ) && ( -x "${USER_HOME}/pre_backup.sh" ) ]]; then
			echo "${BACKUP_TXT}Starting user-defined pre-backup-run script ..."
			${USER_HOME}/pre_backup.sh
	fi
EOF

	echo "${BACKUP_TXT}Compressing ${ENC_TEXT}files listed in ${USER_HOME}/${BACKUP_FILE_NAME}..."
 
 
	if [ ${ENCRYPT_BACKUP} == "yes" ]; then
		tar -cJ --files-from="${USER_HOME}/${BACKUP_FILE_NAME}" 2> /dev/null | openssl smime -encrypt -aes256 -binary -outform D -out "${USER_HOME}/_backup/${TAR_BACKUP_FILENAME}" "${CRYPT_KEY_FILE}"
	else
		tar -cJf  "${USER_HOME}/_backup/${TAR_BACKUP_FILENAME}" --files-from="${USER_HOME}/${BACKUP_FILE_NAME}" 2> /dev/null
	fi
	
	
	
	case ${BACKUP_TYPE} in

	"folder")
		if [ ! -d ${BACKUP_FOLDER} ]; then
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Backup folder doesnt exist!$(tput sgr0)"
			exit 1
		fi

		if [ ! -d ${BACKUP_FOLDER}/${USER} ]; then
			mkdir ${BACKUP_FOLDER}/${USER}
		fi
		
		if [ ${ROTATION_ENABLED} == "yes" ]; then
			if [ "${ROTATION_TYPE}" != "size" ]; then # [!=] size == filecount & combined
				while [ `dir -1 ${BACKUP_FOLDER}/${USER} | wc -l` -ge ${MAX_FILES} ]
				do
					OLDEST=`ls -tr ${BACKUP_FOLDER}/${USER} | head -n 1` #best and safest way to get oldest file?
					echo "${BACKUP_TXT}$(tput setaf 3)Rotation: Max file count reached! Deleting ${OLDEST}!"
					rm "${BACKUP_FOLDER}/${USER}/${OLDEST}"
				done
			fi
			
			if [ "${ROTATION_TYPE}" != "filecount" ]; then # [!=] filecount == size & combined
				while [ `du -c "${BACKUP_FOLDER}/${USER}" "${USER_HOME}/_backup/${TAR_BACKUP_FILENAME}" -b | tail -n 1 | awk '{print $1}'` -ge ${BACKUP_MAX_SIZE} ]
				do
					OLDEST=`ls -tr ${BACKUP_FOLDER}/${USER} | head -n 1` #best and safest way to get oldest file?
					echo "${BACKUP_TXT}$(tput setaf 3)Rotation: Max backup size reached! Deleting ${OLDEST}!$(tput sgr0)"
					rm "${BACKUP_FOLDER}/${USER}/${OLDEST}"
				done
			fi		
		fi
		
		echo "${BACKUP_TXT}Copying backup to ${BACKUP_FOLDER}/${USER}/"
		cp ${USER_HOME}/_backup/${TAR_BACKUP_FILENAME} ${BACKUP_FOLDER}/${USER}/
		chmod u=rws,go= ${BACKUP_FOLDER}/${USER}/${TAR_BACKUP_FILENAME}		
		;;
	"ftp")
		echo "${BACKUP_TXT}Copying backup to ${FTP_HOST}/${FTP_FOLDER}/"


		#test if settings are valid
		wget ftp://${FTP_HOST}/${FTP_FOLDER}/ -q --user=${FTP_USER} --password=${FTP_PASS} -o /dev/null 2>&1

		case $? in
		2)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status 2 - command parse error.$(tput sgr0)"
			exit 1
			;;
		3)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status 3 - I/O error.$(tput sgr0)"
			exit 1
			;;
		4)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status 4 - network error.$(tput sgr0)"
			exit 1
			;;
		5)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status 5 - SSL verification failure.$(tput sgr0)"
			exit 1
			;;
		6)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status 6 - auth error, check user/pass.$(tput sgr0)"
			exit 1
			;;
		7)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: protocol error.$(tput sgr0)"
			exit 1
			;;
		8)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status 8 - server error, check directory/access rights.$(tput sgr0)"
			exit 1
			;;
		0) #no error
			;;
		*)
			echo "${BACKUP_TXT}$(tput setaf 1)Error: Wget exit status $? - generic error.$(tput sgr0)"
			exit 1
		esac

		#no error here, gobby hopes.

ftp -in ${FTP_HOST} << EOF 
user ${FTP_USER} ${FTP_PASS}
cd ${FTP_FOLDER}
binary
put ${USER_HOME}/_backup/${TAR_BACKUP_FILENAME} ${TAR_BACKUP_FILENAME}
bye
EOF
		;;
	*)
		echo "${BACKUP_TXT}$(tput setaf 1)Error: Unknown backup type, valid types are: folder or ftp.$(tput sgr0)"
		exit 1
	esac


	echo "${BACKUP_TXT}Deleting temp files..."
	rm -rf ${USER_HOME}/_backup >/dev/null 2>&1 

	echo "${BACKUP_TXT}Done!"
done

exit 0